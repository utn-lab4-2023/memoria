# Juego de Memoria

Escribir un juego de memoria en javascript

![Ejemplo](memorygame.jpg)

1)  El tablero consiste en una cuadrícula de 4x4 'tarjetas'
2) Cada tarjeta tiene una imagen diferente en el 'frente', y todas tienen la misma imagen en el 'dorso'
3) Inicialmente todas las tarjetas muestran la misma imagen (el 'dorso' de la tarjeta)
4) Cuando se hace click sobre una tarjeta, se muestra la imagen de 'frente'
5) Cuando se descubren dos tarjetas, si las imágenes son iguales se quedan mostrando el frente y ya no responden al Click
6) Si se descubren dos tarjetas diferentes, esperar unos segundos y volver a ponerlas 'boca abajo' mostrando el dorso (ver timer en javascript)
7) Si quedan parejas sin descubrir, volver al punto 4

### Final del juego
Llevar la cuenta de los intentos _fallidos_. Si se llega a una cantidad determinada, se pierde el juego. Si se descubren todas las tarjetas antes de llegar a la cantidad de errores permitida, se gana la partida.

Indicar el final del juego con algún mensaje.

